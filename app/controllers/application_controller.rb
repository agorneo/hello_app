class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
    render html: "¡hello, world!"
  end
  
  def bye
    render html: "have a nice day :)"
  end

  def grhello
    render html: "Καλή μας ημέρα Υπέροχη Γαία! :)"
  end
end
